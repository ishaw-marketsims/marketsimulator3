package MS3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class BidList {
    List<Bid> bidList = new ArrayList<>();
    private Integer numberOfBids;
    
    public BidList(){
        this.numberOfBids = 0;
    }
    
    public void addBid(Integer bidPrice, Buyer buyer){
        this.bidList.add(new Bid(bidPrice, buyer));
        this.numberOfBids++;
    }
    
    public Bid getBid(Integer i){
        return this.bidList.get(i);
    }
/*    
    public Integer getNumberOfBids(){
        return this.numberOfBids;
    }
*/    
    public Integer getBidListSize(){
        return this.bidList.size();
    }
    
    public void deleteBid(Buyer buyerToDelete){
        Iterator<Bid> it = bidList.iterator();
        while(it.hasNext()){
            Bid b = it.next();
            if(b.getBuyer().equals(buyerToDelete))
                it.remove();
        }
        this.numberOfBids--;
    }
    
    @Override
    public String toString(){
        String bidListDetails=new String();
        if(this.numberOfBids!=0){
            for(Bid b : bidList)
                bidListDetails += b.toString();
        }
        else
            bidListDetails+="No bids have been registered on this market.";
        return bidListDetails;        
    }      
    
}
