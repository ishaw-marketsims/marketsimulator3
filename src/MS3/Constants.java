
package MS3;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */

public class Constants {

    private Constants() {} // stop this class from ever being instantiated
    
    public static final Integer LENGTH_OF_NEGOTIATION = 10;      // number of steps in the negotiation
    public static final Integer MAX_TRADE_SIZE = 300;            // most units that can be traded per round
    
}
