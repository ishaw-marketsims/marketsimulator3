package MS3;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class BuyerList {
    List<Buyer> buyerList = new ArrayList<>();
    private Integer numberOfBuyers;
    
    public BuyerList(){
        this.numberOfBuyers = 0;
    }

    public void addBuyer(Integer MAX_PRICE, Integer demandUnits){
        this.buyerList.add(new Buyer(MAX_PRICE, demandUnits, this.numberOfBuyers));
        this.numberOfBuyers++;
    }
    
    public Buyer getBuyer(Integer i){
        return this.buyerList.get(i);
    }
    
    public Integer getBuyerListSize(){
        return this.buyerList.size();
    }

    public void sortBuyers() {
        Collections.sort(this.buyerList);
        Collections.reverse(this.buyerList);     // orders high to low bid prices
    }

    @Override
    public String toString(){
        String agentListDetails=new String();
        if(this.numberOfBuyers!=0){
            agentListDetails+=String.format("%-12s%-13s%-13s\n","REFERENCE","MAX (BID)","DEMAND");
            for(Buyer b : this.buyerList)
                agentListDetails += b.toString() + "\n";
        }
        else
            agentListDetails+="No buyers have been registered on this market.";
        return agentListDetails;        
    }      

}
