package MS3;

/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class Agent implements Comparable<Agent>{
    private final String ref;
    private Integer marketBidPrice;
    private final double risk;

    public Agent(String ref){
        this.ref = ref;
        this.risk = 0.5 + (Math.random() * 0.5);
    }
    
    public String getRef(){
        return this.ref;
    }

    public Integer getMarketBidPrice() {
        return marketBidPrice;
    }

    public void setMarketBidPrice(Integer marketBidPrice) {
        this.marketBidPrice = marketBidPrice;
    }

    public double getRisk() {
        return risk;
    }
    
    public void updateBidPrice(Integer marketPrice) {
        this.marketBidPrice = (marketPrice + this.marketBidPrice) / 2; // simple approach to consensus price
    }
    
    @Override
    public String toString(){
        return String.format("%-12s", ref);
    }      

    @Override
    public int compareTo(Agent o) {
        return this.marketBidPrice.compareTo(o.marketBidPrice);
    }
    
}
