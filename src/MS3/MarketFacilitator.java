package MS3;

/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author ishaw
 */

public class MarketFacilitator {

    private final BuyerList buyerList;
    private final SellerList sellerList;
    private final KnowledgeManager knowledgeManager;
    private BidList bidList;

    public MarketFacilitator(BuyerList buyerList, SellerList sellerList, KnowledgeManager knowledgeManager) {
        this.buyerList = buyerList;
        this.sellerList = sellerList;
        this.knowledgeManager = knowledgeManager;
    }
    
    public String runNegotiation() { // returns a big String report of what happened
        String report = new String();
        this.bidList = new BidList();
        Boolean noBidsAccepted;
        int averageWinningBidPrice = 0;

        // get bids from buyers, populate the bidList
        this.buyerList.sortBuyers();
        for (Integer i = 0; i < this.buyerList.getBuyerListSize(); i++) {
            if (buyerList.getBuyer(i).getDemandUnits() > 0) {//  if the buyer has any demand at all...
                this.bidList.addBid(this.buyerList.getBuyer(i).bidPrice(), this.buyerList.getBuyer(i));
            }
        }
            
        report += "Bids ready: " + this.sellerList.getSellerListSize() + " sellers, " + this.bidList.getBidListSize() + " bids.\n";

        // begin the negotiation        
        for (Integer i = 0; i < Constants.LENGTH_OF_NEGOTIATION; i++) {
            report += "Round " + (i + 1) + " of " + Constants.LENGTH_OF_NEGOTIATION + "\n";
            noBidsAccepted = true;
            Integer totalWinningBidPrices = 0;
            Integer numWinningBids = 0;
            
            // reset the remaining bids to not accepted
            report += "Buyers: ";
            for (Integer bid = 0; bid < this.bidList.getBidListSize(); bid++) {
                this.bidList.getBid(bid).setBidAccepted(false);
                // update the bid prices
                this.bidList.getBid(bid).getBuyer().updateBidPrice(averageWinningBidPrice);
                this.bidList.getBid(bid).setRound(i+1);
                this.knowledgeManager.addBidEvent(this.bidList.getBid(bid).getBuyer().getRef(), this.bidList.getBid(bid).getBuyer().getMarketBidPrice(), i+1);
                report += this.bidList.getBid(bid).getBuyer().getRef()+"("+this.bidList.getBid(bid).getBuyer().getMarketBidPrice() + "), ";
            }
            report += "\n";
            
            // update the sellers bids
            report += "Sellers: ";
            this.sellerList.sortSellers();
            for (Integer seller = 0; seller<this.sellerList.getSellerListSize(); seller++) {
                if(averageWinningBidPrice != 0)
                    this.sellerList.getSeller(seller).updateBidPrice(averageWinningBidPrice);
                this.knowledgeManager.addBidEvent(this.sellerList.getSeller(seller).getRef(), this.sellerList.getSeller(seller).getMarketBidPrice(), i+1);
                report += this.sellerList.getSeller(seller).getRef()+"("+this.sellerList.getSeller(seller).getMarketBidPrice()+"), ";
            }
            report += "\n";
            
            // each seller looks for a bid it can accept
            // if found, it accepts it, locking it for the rest of the round and fulfils as much demand as it can
            for (Integer j = 0; j < this.sellerList.getSellerListSize(); j++) {                                         // for every seller
                if (this.sellerList.getSeller(j).getSupplyUnits() > 0) {                                     // if the seller has supply left to sell
                    for (Integer k = 0; k < this.bidList.getBidListSize(); k++) {                            // for every bid
                        if (!this.bidList.getBid(k).getBidAccepted()) {                                      // if the bid hasn't already been accepted
                            if (this.sellerList.getSeller(j).bidPrice() <= this.bidList.getBid(k).getBidPrice()   // and if the sales price is less or equal to the buyer bid price
                                    && this.sellerList.getSeller(j).getSupplyUnits() > 0) {                  // and another bidder hasn't bought out all the supply
                                this.bidList.getBid(k).setBidAccepted(true);                                 // accept the sale
                                noBidsAccepted = false;                                                 // something was accepted
                                Seller winningSeller = this.sellerList.getSeller(j);                         // (def to tidy up the code)
                                Buyer winningBuyer = this.bidList.getBid(k).getBuyer();                      // (def to tidy up the code)
                                Integer winningBidPrice = (winningSeller.bidPrice() + winningBuyer.bidPrice()) / 2; // set the unit price
                                totalWinningBidPrices += winningBidPrice;                               // keep track of the winning prices
                                numWinningBids++;                                                       // need this to find the average later
                                Integer salesVolume = doTrade(winningBuyer, winningSeller, winningBidPrice);        // do the trade, find out the amount traded
                                report += "Buyer " + winningBuyer.getRef() + " buys " + salesVolume + " from Seller " + winningSeller.getRef() + " at winning price: " + winningBidPrice + "\n";
                            }
                        }
                    }
                }
            }
            if (noBidsAccepted) {
                report += "No bids accepted this round.\n";
            } else {
                averageWinningBidPrice = (totalWinningBidPrices / numWinningBids);
                report += "Average winning bid price: " + averageWinningBidPrice + "\n\n";
            }

        }
        
        // restore seller supply
        for (Integer i = 0; i < this.sellerList.getSellerListSize(); i++)
            this.sellerList.getSeller(i).setSupplyUnits(this.sellerList.getSeller(i).getOriginalUnits());
        
        return report;
    }
    
    public Integer doTrade(Buyer winningBuyer, Seller winningSeller, Integer winningBidPrice) {
        // winningBuyer and winningSeller agreed to trade at winningBidPrice
        Integer salesVolume = Math.min(Math.min(winningBuyer.bidAmount(), winningSeller.bidAmount()), Constants.MAX_TRADE_SIZE);
        winningSeller.addMoneyMade(winningBidPrice * salesVolume);
        winningSeller.setSupplyUnits(winningSeller.bidAmount() - salesVolume);
        winningBuyer.addMoneySpent(winningBidPrice * salesVolume);
        winningBuyer.setDemandUnits(winningBuyer.bidAmount() - salesVolume);
        if(winningBuyer.bidAmount() == 0)
            this.bidList.deleteBid(winningBuyer);
        return salesVolume;
    }
    
}
