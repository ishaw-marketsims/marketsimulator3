package MS3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class SellerList {
    List<Seller> sellerList = new ArrayList<>();
    private Integer numberOfSellers;
    
    public SellerList(){
        this.numberOfSellers = 0;
    }

    public void addSeller(Integer MIN_PRICE, Integer supplyUnits){
        this.sellerList.add(new Seller(MIN_PRICE, supplyUnits, this.numberOfSellers));
        this.numberOfSellers++;
    }
   
    public Seller getSeller(Integer i){
        return this.sellerList.get(i);
    }
    
    public Integer getSellerListSize(){
        return this.sellerList.size();
    }
    
    public void sortSellers() {
        Collections.sort(this.sellerList);
    }
    
    @Override
    public String toString(){
        String agentListDetails=new String();
        if(this.numberOfSellers!=0){
            agentListDetails+=String.format("%-12s%-13s%-13s\n","REFERENCE","MIN (BID)","SUPPLY");
            for(Seller s : this.sellerList)
                agentListDetails += s.toString() + "\n";
        }
        else
            agentListDetails+="No sellers have been registered on this market.";
        return agentListDetails;        
    }      
   
}
