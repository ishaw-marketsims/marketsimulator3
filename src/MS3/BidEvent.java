package MS3;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */
public class BidEvent {

    private String bidAgentRef;
    private Integer bidValue;
    private Integer bidRound;

    public BidEvent(String bidAgentRef, Integer bidValue, Integer bidRound) {
        this.bidAgentRef = bidAgentRef;
        this.bidValue = bidValue;
        this.bidRound = bidRound;
    }

    public String getBidAgentRef() {
        return bidAgentRef;
    }

    public void setBidAgentRef(String bidAgentRef) {
        this.bidAgentRef = bidAgentRef;
    }

    public Integer getBidValue() {
        return bidValue;
    }

    public void setBidValue(Integer bidValue) {
        this.bidValue = bidValue;
    }

    public Integer getBidRound() {
        return bidRound;
    }

    public void setBidRound(Integer bidRound) {
        this.bidRound = bidRound;
    }

    @Override
    public String toString() {
        return "Ref: " + this.bidAgentRef + ", bid: " + this.bidValue + ", round: " + this.bidRound;
    }
    
}
